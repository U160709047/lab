public class FindPrimes {

    public static void main(String args[]) {
      
     int limit = Integer.parseInt(args[0]);

      for(int number = 2; number<limit; number++){
      
          if(isPrime(number)){
              System.out.print(number + " ");
          }
      }

    }

     public static boolean isPrime(int number){
        for(int i=2; i<number; i++){
           if(number%i == 0){
               return false; 
           }
        }
        return true; 
    }
}

