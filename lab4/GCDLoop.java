public class GCDLoop{
  public static void main(String[] args){
    int k = Math.max(Integer.parseInt(args[0]),Integer.parseInt(args[1]));    
    int l = Math.min(Integer.parseInt(args[0]),Integer.parseInt(args[1]));     
    for (int m = k % l; m != 0; m = k % l){
      k = l;
      l = m;
    }
    System.out.println(l);
  }
}
