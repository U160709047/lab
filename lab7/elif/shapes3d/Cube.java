package elif.shapes3d;
import elif.shapes.Square;

public class Cube extends Square {
	public Cube(int side) {
		super(side);
		
	}
	public Cube() {
		side = super.side;
	}
	public int area() {
	
		return 6* super.area();
		
}
	public int volume() {
		return super.side * super.area();
		
	}
	public String toString() {
		return "Side of the cube is= " + side;
	}
}

