
public class GreatestDemo {

    public static void main(String[] args){

    	int value1 = Integer.parseInt(args[0]);
		int value2 = Integer.parseInt(args[1]);
		int value3 = Integer.parseInt(args[2]);
        int first_result ;
        int last_result ;

        first_result = value1 > value2 ? value1 : value2 ;
        last_result = value3 > first_result ? value3 : first_result;
        // ? --> if : -->else


        System.out.println(last_result);	

        }
    }