package elif.main;
import java.util.ArrayList;

import elif.shapes.Circle;
import elif.shapes3d.Cube;
import elif.shapes3d.Cylinder;
public class Test3D {
	public static void main(String[] args) {
		ArrayList<Cylinder> cylinders = new ArrayList<>();
		Cylinder cylinder = new Cylinder();
		
		System.out.println(cylinder);
		
		cylinders.add(cylinder);
		cylinders.add(new Cylinder(6,7));
		System.out.println(cylinders);
		
		Cube cube = new Cube(5);
		System.out.println(cube);
	
	}
}
