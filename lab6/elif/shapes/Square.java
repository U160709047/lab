package elif.shapes;

public class Square {
	public int side;
	
	public Square() {
		side = 9;
	}
	public Square(int side) {
		this.side= side;
		System.out.println("Square is being created");
	}
	public int area() {
		return side*side;
	}
}
